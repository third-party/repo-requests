The template is for requesting a repository to host a fork of an upstream
project. These projects require patches in order to be used before embedding
their sources into Kitware projects (usually via the `update.sh` mechanism).

---
Name: NAME

Upstream repository: UPSTREAM URL

Hosting project(s) (e.g., `vtk/vtk` or `paraview/paraview`):

  - PROJECT LIST

Maintainers of the fork:

  - MAINTAINER LIST

Tasks:

  - [ ] Create repository (`third-party/NAME`).
  - [ ] Run `./prepare-repo.sh fork NAME`

Cc: @utils/maintainers/third-party
