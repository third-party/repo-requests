The template is for requesting a repository to be mirrored from an upstream to
Gitlab. This is mainly useful for when a customer needs access to a Git
repository, but are firewalled from reaching sites other than Kitware
infrastructure.

---
Name: NAME

Upstream repository: UPSTREAM URL

  - [x] mirror branches:
    - [x] `master`
    - [ ] `next`
    - [ ] `OTHER`
  - [x] mirror all tags
  - [ ] mirror extra refs:
    - `CUSTOM_REFS`

Tasks:

  - [ ] Create repository (`third-party/NAME`).
  - [ ] Run `./prepare-repo.sh mirror NAME`

Cc: @utils/maintainers/third-party
