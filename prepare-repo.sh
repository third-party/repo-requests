#!/bin/sh

# Run this script to set the feature flags for a third party repository.
#
# Usage:
#     ./prepare-repo.sh <fork|mirror> <name>
#
# Where `<name>` completes `https://gitlab.kitware.com/third-party/<name>` for
# the project in question.
#
# Set up an API token in the `.token` file for use with this script.

set -e

readonly repotype="$1"
shift

case "$repotype" in
    fork)
        extra_disables=
        ;;
    mirror)
        extra_disables="--data merge_requests_access_level=disabled --data issues_access_level=disabled"
        ;;
    *)
        echo >&2 "Unknown repository type: $repotype"
        exit 1
        ;;
esac
readonly extra_disables

readonly name="$1"
shift

token="$( cat .token )"
readonly token

curl -X PUT \
    --header "PRIVATE-TOKEN: $token" \
    --data analytics_access_level=disabled \
    --data builds_access_level=disabled \
    --data container_registry_access_level=disabled \
    --data environments_access_level=disabled \
    --data feature_flags_access_level=disabled \
    --data infrastructure_access_level=disabled \
    --data lfs_enabled=false \
    --data model_experiments_access_level=disabled \
    --data model_registry_access_level=disabled \
    --data monitor_access_level=disabled \
    --data pages_access_level=disabled \
    --data releases_access_level=disabled \
    --data requirements_access_level=disabled \
    --data security_and_compliance_access_level=disabled \
    --data service_desk_enabled=false \
    --data snippets_access_level=disabled \
    --data wiki_access_level=disabled \
    $extra_disables \
    "https://gitlab.kitware.com/api/v4/projects/third-party%2f${name}"
