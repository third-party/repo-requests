# Repository requests

Please file issues on this repository for requests to add mirrors or forks of
third party projects. There are templates for use in filing issues.

# Creating a repository

## Fork

Description: `Fork of $name upstream: $url`

Settings:

  - Disable LFS
  - Disable CI/CD
  - Disable container registry
  - Disable analytics
  - Disable security and compilance
  - Disable wiki
  - Disable snippets
  - Disable package registry
  - Disable model experiments
  - Disable model registry
  - Disable monitor
  - Disable environments
  - Disable feature flags
  - Disable infrastructure
  - Disable releases
  - Disable service desk

Add maintainer list as `Master` permissions.

## Mirror

Description: `Mirror of $name: $url`

Settings:

  - Disable all settings for a fork
  - Disable merge requests
  - Disable issues

Add configuration to `kwrobot01` mirroring setup (ask Brad and Ben).
